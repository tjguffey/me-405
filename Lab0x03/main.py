'''
@file mainADC.py

This file acts as the main file run on the Nucleo that will serve as an ADC.

This lab focuses on using the adc available on the Nucleo to read and process 
changes in voltage on the board's pins. This particular file will read, save,
and send the data to a computer after a character is received.

@author Tyler J. Guffey

@copyright TG-Coding

@date January 30, 2021
'''


import pyb, array
from pyb import UART

## The serial port of sorts, the register for input values
myuart = UART(2)

## The intended frequency to take data points
frequency = 20

## Number of points to take while looking for transition
data_points = 40

## The voltage change we're looking to register as a "good" transition
good_trans = 0.5 #volts

## The conversion from adc 'counts' to volts
conv = 3.3/4095 #V/count

## Continuously run this code until cntrl+c is pressed
while True:
    ## If there is anything in the register, read it as val
    if myuart.any() != 0:
        val = myuart.readchar()
        ## If the read value is capital G, continue with the ADC code
        if val == 71:                           #if value entered is G in ASCII
            mock_sensor = pyb.Pin.board.PA0
            adc = pyb.ADC(mock_sensor)          # create an analog object from a pin
            tim = pyb.Timer(6, freq=frequency)  #Timer on Channel 6, 50 Hz
            buf = array.array('H', data_points*[0x7FFFF])  # array with space for 40 data points
            adc.read_timed(buf,tim) #read the adc counter using the timer and enter the values into the array
            minimum = min(buf)
            maximum = max(buf)
            ## If the max-min value indicates a good jump, transmit the values
            if maximum-minimum > good_trans/conv:
                for n in range (len(buf)):
                    #buf[n] = int(buf[n]*conv)
                    print(buf[n])  #Printing I believe transmits the values to the serial line, use ser.readline() to obtain

            
        ## If they don't enter a capital G, tell them what to do    
        else:
            myuart.write('Please enter a G or leave me alone')

