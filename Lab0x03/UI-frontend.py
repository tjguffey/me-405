'''
@file UI_frontend.py

This file serves as the middle ground between the Nucleo and Spyder

Interestingly, this code will send a character to the Nucleo, await a response,
register the response and write it to a .csv file, read that .csv file, and
provide a Matlab-esque plot representing the data as it was taken (ideally). In
order to properly use this code as originally intended, the user must be aware
of and use the same 'frequency' and 'data_points' values for both the main.py
file and this one.

@author Tyler J. Guffey

@copyright TG-Coding

@date January 30, 2021
'''

import serial, time, csv
from matplotlib import pyplot as plt


def sendChar():
    '''
    @brief    Takes an input and sends it through the serial line
    '''
    inv = input('Genie does as you wish: ') #Waits for user input
    ser.write(str(inv).encode('ascii'))  #Writes the user input to the serial line (Can be read with UART on the Nucleo)


def plot4u(one,two):
    '''
    @brief     Plots a graph with the format specified
    '''
    plt.figure()
    plt.plot(one, two, 'ro')
    plt.title('ADC Data')
    plt.xlabel('Time [ms]')
    plt.ylabel('ADC Readig [V]')
    plt.show()


## Define the serial port we will use, empty the register
ser = serial.Serial(port='/dev/tty.usbmodem14103',baudrate=115273,timeout=1)
ser.flushInput()

## Index used to change the time input into the csv file
times_index = 0

## The frequency of readings, must be the same as the main.py file for accuracy
frequency = 20 #Hz

## Convert seconds to ms
conv = 1e3 #ms/s

## Convert the counts to volts
conv2 = 3.3/4095 #volts/counts

## Number of data points to take, must be the same as the main.py***
data_points = 40 

## Initially empty time array
times = list()

## The time we'll take before we cancel the process
lag_out = 20 #sec



## Send the initial character input to the Nucleo
for n in range(1):
    sendChar()   #Prints the Character once
 
    
## The time at which we begin caring whats going on
start = time.time()  # sec from 1984 or whenever the nerds figured it out

## The time we will allow the Nucleo between data send
wait_time = 2  #sec


## For loop to create the times of each point taken
for n in range(data_points):
    times.append(float(n/frequency*conv))


## Run this code until broken out of with the below conditions
while True: 
    ## If the serial line has anything in it, read it and decode it
    if ser.readline():
        results = ser.readline()
        floated = float(results[0:len(results)].decode("ASCII"))
        current_time = times[times_index]
        
        ## If its the first data sent back, erase the old csv file and write to a new one
        if times_index == 0:
            with open("adc_test.csv", 'w') as file:  #using "a" instead of 'w' will append to the file instead of replacement
                writer = csv.writer(file, delimiter=",")
                writer.writerow([current_time, floated])
        ## If the data received isn't the first back in this run, append the new values to the existing .csv file
        else:
            with open("adc_test.csv", 'a') as file:  #using "a" instead of 'w' will append to the file instead of replacement
                writer = csv.writer(file, delimiter=",")
                writer.writerow([current_time, floated])
        ## Its important for both the .csv and timeout conditions that we keep track of our index
        times_index += 1
        start = time.time()
     
    ## If the program hasnt sent anything *more* back in 2 seconds, consider it done
    elif time.time()-start > wait_time and times_index>0:
        print('Data Collection Complete:')
        break
    
    ## If the program doesn't return anything in 20 seconds, break out
    elif time.time()-start > lag_out:
        print('The current plot will display past values because new data was not aquired:')
        break
    

## Create two new lists to fill with data
time_list = list()
data_list = list()

## Open the .csv file
file = open('adc_test.csv')

## Create a readout of all the csv lines
myRows = file.readlines()

## Fill the two lists with all of our data
for myRow in myRows:
    
    ## Gain only the relevant data characters
    myList = myRow.strip('\n').split(',')
    
    ## Append the time and data to their appropriate lists, change counts to volts
    time_list.append(float(myList[0]))
    data_list.append(float(myList[1]*conv2))

## Close the file
file.close()

## Plot the lists we just made with formatting specified in the plot4u command
plot4u(time_list, data_list)

## Close the serial line
ser.close()