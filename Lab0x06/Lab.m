%% Lab0x06: Simulation or Reality?
% ME 405 Winter 2021 - Laboratory Assignment #6
%
% *Author:* Tyler Guffey, Tatum Yee
%
% California Polytechnic State University, San Luis Obispo, CA
%
% *Date Created:* 2/21/21
%
% *Description:*
%
% This lab was used to analyze our platform system and the effects of a
% controller in regulating it. 
%
% *Required Files:*
% Lab6sim.slx
%
%% Housekeeping
% Here we are just clearing previous data from variables and closing all
% previous figures.
clc;
clearvars;  %clear all
close all;

%% Variable Setup
% Here, we will be inputting variable for our simulink file to work. For our analysis,
% we have to define the state of the moving quarter car, and the constants
% used for the springs and dampeners.

r_m = 0.06;         %[m] Radius of lever arm
l_r = 0.05;         %[m] Length of push rod
r_b = 0.0105;       %[m] Radius of ball
r_g = 0.042;        %[m] Verical distance from U-Joint to CG
l_p = 0.110;        %[m] Horizontal distance from U-Joint to Push-Rod Pivot
r_p = 0.0325;       %[m] Verical distance from U-Joint to Push-Rod Pivot
r_c = 0.05;         %[m] Verical distance from U-Joint to Platform Surface
m_b = 0.03;         %[kg] Mass of Ball
m_p = 0.4;          %[kg] Mass of Platform
I_p = 1.88e6*1e-9;  %[kg-m^2] Moment of Inertia of Platform
b = 0.01;           %[Nm-s/rad] Viscous Friction at U-Joint
I_b = 2/5*m_b*r_b^2;%[kg-m^2] Moment of Inertia of Ball
g = 9.81;           %[m/s^2] Acceleration of Gravity

%% Linearization of Lab 5 Results
% The code below reorganizes and formats our values derived from lab five
% into a steady state system format. We use the derived A, B, C, and D
% matrixes in the Simulink model which is also in the bitbucket repository.

syms th th_d x x_d T_m 

% M Matrix (Provided from Lab 5)
M = [-1*(m_b*r_b^2+m_b*r_c*r_b+I_b)/r_b, -1*(I_b*r_b+I_p*r_b+m_b*r_b^3+m_b*r_b*r_c^2+2*m_b*r_b^2*r_c+m_p*r_b*r_g^2+m_b*r_b*x^2)/r_b;
     -1*(m_b*r_b^2+I_b)/r_b, -1*(m_b*r_b^3+m_b*r_c*r_b^2+I_b*r_b)/r_b];
 
% f Matrix (also provided)
f = [b*th_d-g*m_b*(sin(th)*(r_b+r_c)+x*cos(th))+T_m*l_p/r_m+2*m_b*th_d*x*x_d-g*m_p*r_g*sin(th)
     -m_b*r_b*x*th_d^2-g*m_b*r_b*sin(th)];

a_dd = M\f;

g = [x_d
     th_d
     a_dd(1)
     a_dd(2)];

v_a = [x
       th
       x_d
       th_d];
%%
% Jacobian with respect to pos, ang pos...
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

% Jacobian with respect to Torque
B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

% Identity Matrix
C = eye(4);

% Empty Matrix of Zeros
D = zeros(4,1);
%%      
Gains = [-0.05 -0.02 -0.3 -0.2];

length = 0.4;   %[s]

%simulation = sim('Lab6sim');

%% Plot the Results 
% Theres a few plots that we need... These will be generated below.

% Case 3A
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

C = eye(4);

D = zeros(4,1);

initial = [0
           0
           0
           0];
step = 0;       
simulation = sim('Lab6sim');

figurename = 'Question 3A'; % Name of Saved Image
figure('Name',figurename,'NumberTitle','off',...
       'units','inches') % generate figure - set inch units for size
plot(simulation.tout,simulation.simout(:,1),'k-',simulation.tout,simulation.simout(:,2),'k--', simulation.tout,simulation.simout(:,3),'k.-',simulation.tout,simulation.simout(:,4),'k*');
legend('Position [m]','Angular Postion [rad]','Velocity [m/s]','Angular Velocity [rad/s]');
xlabel({'Time, {\it t} [sec]'
        ''
        '\bfFigure 1: \rmInitial [0,0,0,0]'});
ylabel('Response, {\it q} []');
print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpi

% Case 3B
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

C = eye(4);

D = zeros(4,1);

initial = [0
           0
           0.05
           0];
step = 0;
simulation = sim('Lab6sim');
figurename = 'Question 3B'; % Name of Saved Image
figure('Name',figurename,'NumberTitle','off',...
       'units','inches') % generate figure - set inch units for size
plot(simulation.tout,simulation.simout(:,1),'k-',simulation.tout,simulation.simout(:,2),'k--', simulation.tout,simulation.simout(:,3),'k.-',simulation.tout,simulation.simout(:,4),'k*');
legend('Position [m]','Angular Postion [rad]','Velocity [m/s]','Angular Velocity [rad/s]');
xlabel({'Time, {\it t} [sec]'
        ''
        '\bfFigure 2: Initial [0,0,0.5,0]\rm'});
ylabel('Response, {\it q} []');
print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpi

% Case 3C
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

C = eye(4);

D = zeros(4,1);

initial = [0
           0
           0
           deg2rad(5)];
step = 0;
simulation = sim('Lab6sim');
figurename = 'Question 3C'; % Name of Saved Image
figure('Name',figurename,'NumberTitle','off',...
       'units','inches') % generate figure - set inch units for size
plot(simulation.tout,simulation.simout(:,1),'k-',simulation.tout,simulation.simout(:,2),'k--', simulation.tout,simulation.simout(:,3),'k.-',simulation.tout,simulation.simout(:,4),'k*');
legend('Position [m]','Angular Postion [rad]','Velocity [m/s]','Angular Velocity [rad/s]');
xlabel({'Time, {\it t} [sec]'
        ''
        '\bfFigure 3: \rmInitial [0,0,0,5]'});
ylabel('Response, {\it q} []');
print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpi

% Case 3D
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

C = eye(4);

D = zeros(4,1);

initial = [0
           0
           0
           0];
step = 0.001;
simulation = sim('Lab6sim');
figurename = 'Question 3D'; % Name of Saved Image
figure('Name',figurename,'NumberTitle','off',...
       'units','inches') % generate figure - set inch units for size
plot(simulation.tout,simulation.simout(:,1),'k-',simulation.tout,simulation.simout(:,2),'k--', simulation.tout,simulation.simout(:,3),'k.-',simulation.tout,simulation.simout(:,4),'k*');
legend('Position [m]','Angular Postion [rad]','Velocity [m/s]','Angular Velocity [rad/s]');
xlabel({'Time, {\it t} [sec]'
        ''
        '\bfFigure 4: \rmInitial [0,0,0,0] Plus Torque'});
ylabel('Response, {\it q} []');
print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpi

% Case 4
A = jacobian(g, v_a);
A = subs(A, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
A = double(A);

B = jacobian(g, T_m);
B = subs(B, [x,th,x_d,th_d,T_m], [0,0,0,0,0]);
B = double(B);

C = eye(4);

D = zeros(4,1);

initial = [0
           0
           0.5
           0];
step = 0;
length = 20;
simulation = sim('Lab6sim');
figurename = 'Question 4'; % USER TO MODIFY
figure('Name',figurename,'NumberTitle','off',...
       'units','inches')
plot(simulation.tout,simulation.simout1(:,1),'k-',simulation.tout,simulation.simout1(:,2),'k--',simulation.tout,simulation.simout1(:,3),'k.-',simulation.tout,simulation.simout1(:,4),'k*'); 
legend('potision [m]','angular postion [rad]','velocity [m/s]','angular velocity [rad/s]');
xlabel({'Time, {\it t} [sec]'
        '' 
        '\bfFigure 5: \rmClosed Loop Controller'});
ylabel('Response, {\it q} []');
print(gcf,figurename,'-djpeg','-r600');

%% Discussion
% Each of the presented plots above are meant to represent our system in
% varied cases. Each of them does well in keeping the balls change in position
% minimal. One thing that stands out in all cases is the fact that the
% angular displacement of the platform seems to be the most impacted; or at
% least it varies the most for every case. This makes alot of sense,
% because in order to adjust for the ball's movement, the platform will 
% need to move. In most cases, the balls speed stays minimal,
% which is also realistic, as the ball starts from rest in all cases.
% Lastly, for part 4, we were intended to create a closed loop system with
% a controller. While this doesn't sound much different, our plot looks way
% different, which it should. If you look closely, the balls position
% oscillates but always stays close to center. The platform generally
% oscillates too to maintain this balance, and this indicates to us that
% the system's control is working properly. 


