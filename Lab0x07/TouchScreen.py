'''
@file TouchScreen.py

This file serves as the Touch Screen Class, and is currently only used to 
monitor the activity on the resistance based touch screen used for the class 
term project.

@author Tyler J. Guffey, Tatum X. Yee

@copyright TG-Coding

@date March 1, 2020
'''

import pyb, utime
class TouchScreen:
    ''' This class implements a receiver for a touchscreen '''
    
    def __init__ (self, xp, xm, yp, ym, l, w, c, multx, multy): 
        ''' 
        @brief Creates a TouchScreen Object that can read position
        @param xp   xp pin of touchscreen
        @param xm   xm pin of touchscreen
        @param yp   yp pin of touchscreen
        @param ym   ym pin of touchscreen
        @param l    Length of touchscreen
        @param w    Width of touchscreen
        @param c    Center of touchscreen
        @param multx Constant multiplier for x position correction
        @param multy Constant multiplier for y position correction
        '''
        ## Pin xp
        self.xp = xp
        
        ## Pin xm
        self.xm = xm
        
        ## Pin yp
        self.yp = yp
        
        ## Pin ym
        self.ym = ym
        
        ## Length of touch screen
        self.l = l
        
        ## Width of touch screen
        self.w = w
        
        ## Center of touch screen
        self.c = c
        
        ## Multiplier for x position reading
        self.multx = multx
        
        ## Multiplier for y position reading
        self.multy = multy
        
    def xScan (self):
        '''
        @brief     This will scan the position with x components energized
        '''
        ## The xp pin set as a high push pull pin
        self.xp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The yp pin set as a floating push pull pin
        self.yp.init(mode=pyb.Pin.IN)
        
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=pyb.Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count
        ymADC = pyb.ADC(self.ym)
        
        ## Pause for 4ms
        utime.sleep_ms(4)
        
        ## Position on the x axis
        xPos = (ymADC.read()*self.multx)-self.c[0]
        
        return(xPos)
        print('X distance: ' + str(xPos))
    
    
    ## Turns the motor off
    def yScan (self):
        '''
        @brief     This will scan the position with y components energized
        '''
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The ym pin set as a low push pull pin
        self.ym.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The xp pin set as a floating push pull pin
        self.xp.init(mode=pyb.Pin.IN)
        
        ## The xm pin set as an analog pin that will be measured
        self.xm.init(mode=pyb.Pin.ANALOG)
        
        ## The xm pin used as an ADC to read the ADC count
        xmADC = pyb.ADC(self.xm)
        
        ## Pause for 4ms
        utime.sleep_ms(4)
        
        ## Position on the y axis
        yPos = -(xmADC.read()*self.multy)+self.c[1]
        
        return(yPos)
        print('Y distance: ' + str(yPos))
    
    def zScan (self):
        ''' 
        @brief       This will set the duty used by the motor
        '''
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=pyb.Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count?
        ymADC = pyb.ADC(self.ym)
        
        ## Pause for 4ms
        utime.sleep_ms(4)
        
        ## Reads ADC count on the y axis
        zTouch = ymADC.read()
        print('Wazzah ' + str(zTouch))
        
        ## In between high and low state, return True
        if zTouch < 4000:
            #print('Get off of me!')
            return True
        ## In high state so no touch detected, return False
        else:
            #print('I wish I could feel something...I feel...cold')
            return False
        
    def readscreen(self):
        '''
        @brief     Reads all positions and returns x and y locations
        '''
        ## If something is touching the screen scan and return position
        if self.zScan():
            x = self.xScan()
            y = self.yScan()
            z = self.zScan()
            data = (x,y,z)
            return(data)
        ## If not, say something like 'this doesn't make sense'
        else:
            print('PLease Touch Screen') #('There is nothing to read... or maybe your code sucks. Could be either, really. My bet is the latter')
        
if __name__ == '__main__':
    # Define the xp, xm, yp, ym pins
    xp = pyb.Pin.cpu.A7
    xm = pyb.Pin.cpu.A1
    yp = pyb.Pin.cpu.A0
    ym = pyb.Pin.cpu.A6
    
    # Define the board dimensions
    l = 186 #mm
    w = 86 #mm
    c = (l/2, w/2)
    
    # Define Calibration Gains
    multx = l/4080
    multy = w/4080
    
    ## Create a TouchScreen Object to call and test
    moe = TouchScreen(xp,xm,yp,ym,l,w,c,multx,multy)
    
    ## Create a method to test for a position every few seconds
    while True:
        try:
            # Return a reading on the screen
            moe.readscreen()
            # Wait 2 seconds before trying again
            utime.sleep(2)
    
        except KeyboardInterrupt:
            print('Control+C has been pressed, hope that worked')
            break