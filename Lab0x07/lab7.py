## @file lab7.py
# This file creates a class for to scan for the position of the location at which
# a touchscreen is touched.
#
# The four functions for this class are init, xScan, yScan, zScan, readings, and timeAvg
#
# File Link:
# @author Tatum X. Yee and Tyler J. Guffey
# @date March 9, 2021
#

from pyb import Pin 
from pyb import ADC
import pyb, utime

class scanTouchScreen:
    '''
    @brief Initalizes values and then scans for the location on the x and y axis
           Will also show whether the touchscreen has been pressed
    '''
    ## Initialize the Scanner module
    def __init__ (self, xp, xm, yp, ym, l, w, c, multx, multy): 
        ''' 
        @brief Creates a TouchScreen Object that can read position
        @param xp   xp pin of touchscreen
        @param xm   xm pin of touchscreen
        @param yp   yp pin of touchscreen
        @param ym   ym pin of touchscreen
        @param l    Length of touchscreen
        @param w    Width of touchscreen
        @param c    Center of touchscreen
        @param multx Constant multiplier for x position correction
        @param multy Constant multiplier for y position correction
        '''
        ## Pin xp
        self.xp = xp
        
        ## Pin xm
        self.xm = xm
        
        ## Pin yp
        self.yp = yp
        
        ## Pin ym
        self.ym = ym
        
        ## Length of touch screen
        self.l = l
        
        ## Width of touch screen
        self.w = w
        
        ## Center of touch screen
        self.c = c
        
        ## Multiplier for x position reading
        self.multx = multx
        
        ## Multiplier for y position reading
        self.multy = multy
        
        ## Index for keeping track of reading times
        self.time = 0
        
        ## How many times we've taken readings without averaging time
        self.iterations = 0
        
    ## Scan the x direction for current location
    def xScan(self):
        ## Time this command is started at
        start_x = pyb.micros()
        
        ## The xp pin set as a high push pull pin
        self.xp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The yp pin set as a floating push pull pin
        self.yp.init(mode=pyb.Pin.IN)
        
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=pyb.Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count
        ymADC = pyb.ADC(self.ym)
        
        ## Avoid Nonlinear response by waiting 4 us
        utime.sleep_us(4)
        
        ## Position on the x axis
        xPos = (ymADC.read()*self.multx) - self.c[0]
        
        ## Time after the location is actually scanned
        end_x = pyb.elapsed_micros(start_x)
        
        ## Update the time index
        self.time += end_x
        
        ## Print the Actual Time the xScan command took
        print('xScan time 1: ' + str(end_x))
        
        ## Return the x position obtained from the scan
        return(xPos)
    
    ## Scan the screen for y location    
    def yScan(self):
        ## Time this command is started at
        start_y = pyb.micros()
        
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The ym pin set as a low push pull pin
        self.ym.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The xp pin set as a floating push pull pin
        self.xp.init(mode=pyb.Pin.IN)
        
        ## The xm pin set as an analog pin that will be measured
        self.xm.init(mode=pyb.Pin.ANALOG)
        
        ## The xm pin used as an ADC to read the ADC count
        xmADC = ADC(self.xm)
        
        ## Avoid Nonlinear response by waiting 4 us
        utime.sleep_us(4)
        
        ## Position on the y axis
        yPos = -(xmADC.read()*self.multy)+self.c[1]
        
        ## Time after the location is actually scanned
        end_y = pyb.elapsed_micros(start_y)
        
        ## Update the time index
        self.time += end_y
        
        ## Print the Actual Time the yScan command took
        print('yScan time: ' + str(pyb.elapsed_micros(start_y)))
        
        ## Return the x position obtained from the scan
        return(yPos)
    
    ## Scan the screen to determine if someone is touching it    
    def zScan(self):
        ## Time this command is started at
        start_z = pyb.micros()
        
        ## The yp pin set as a high push pull pin
        self.yp.init(mode=pyb.Pin.OUT_PP, value=1)
        
        ## The xm pin set as a low push pull pin
        self.xm.init(mode=pyb.Pin.OUT_PP, value=0)
        
        ## The ym pin set as an analog pin that will be measured
        self.ym.init(mode=pyb.Pin.ANALOG)
        
        ## The ym pin used as an ADC to read the ADC count?
        ymADC = pyb.ADC(self.ym)
        
        ## Pause for 4ms
        utime.sleep_ms(4)
        
        ## Reads ADC count on the y axis
        zTouch = ymADC.read()
        print('Wazzah ' + str(zTouch))
        
        ## Time after the location is actually scanned 
        end_z = pyb.elapsed_micros(start_z)
        
        ## Update the time index
        self.time += end_z
        
        ## In between high and low state, return True
        if zTouch < 4000:
            #print('Get off of me!')
            return True
        ## In high state so no touch detected, return False
        else:
            #print('I wish I could feel something...I feel...cold')
            return False
        print('zScan time: ' + str(end_z))
    
    ## Take a complete reading if something is touching the screen
    def readings(self):
        if self.zScan() == True:
            x = self.xScan()
            y = self.yScan()
            z = self.zScan()
            self.iterations +=1
            print(x,y,z)
        else:
            print('Please touch the screen.')
        
    def timeAvg(self):
        '''
        @brief Determine average time it takes to completely scan the screen
        '''
        ## Again, how many times we want to run the readings()
        self.iterations = iterations
        
        ## The average time taken per iteration
        time_avg = self.time/self.iterations  
        
        ## Print how much the average time was
        print('Average Total Reading Time: ' + str(time_avg))
        
        ## Reset the iteration index
        self.iterations = 0
        
        ## Reset the time index
        self.time = 0
       
if __name__ == '__main__':   
    # Assign Pins
    xp = Pin.cpu.A7
    xm = Pin.cpu.A1
    yp = Pin.cpu.A0
    ym = Pin.cpu.A6
    
    # Assign Touch Screen Dimensions
    l = 186 # mm
    w = 86 # mm
    c = (l/2, w/2)
    
    # Assign Gain Values
    multx = l/4080
    multy = w/4080
    
    ## Assign the task to run it
    scanDatScreen = scanTouchScreen(xp, xm, yp, ym, l, w, c, multx, multy)
    
    ## Number of runs for test
    iterations = 100
    
    ## Run it back G
    for i in range(iterations):
        # Return a reading on the screen
        scanDatScreen.readings()
        #scanDatScreen.putEverythingTogetherForOptimization()
        
        utime.sleep(2)
        
    scanDatScreen.timeAvg()   