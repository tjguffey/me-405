'''
@file Lab0x01.py

This file serves as the main function file that will take inputs from the
user as the amount given and the cost this was intended to cover and respond 
with the appropriate amount of change.

This code will also send the change required back to the user based on the 
input. The FSM that this code uses is shown in the attached image.

@image html HW0x01.png

@author Tyler J. Guffey

@copyright TG-Coding

@date January 9, 2021
'''
import keyboard, math

## Empty the key roster
pushed_key = None

state = 0
entered = []
need = 0
money = (20,10,5,1,0.25,0.10,0.05,0.01)

def on_keypress (thing):
    """ 
    Callback which runs when the user presses a key.
    
    This command checks what key is pressed and saves the key as it is pushed.
    """
    global pushed_key

    pushed_key = thing.name

def getChange(change):
    '''
    Command that returns change
    
    This function returns the amount of certain curreny types to satisfy the
    inputted value 'change'
    '''
    distchange = []
    ## Run State 0 Code
    if change<0:
        need = -1*change
        if entered[-1]<0:
            entered.append(-1*entered[-1])
        else:
            pass
        print('Thanks, but you still owe: ', str(need), ' for this item. Please try again.')
        
    else:
        print('Your change today will be ', str(change), ' and will be distributed as: ')
        need = 0
        for x in range(len(money)):
            distchange.append(math.floor(change/money[x]))
            change = change-distchange[x]*money[x]

        print(str(distchange[0]), ' twenty dollar bills, ',
              str(distchange[1]), ' ten dollar bills, ',
              str(distchange[2]), ' five dollar bills, ',
              str(distchange[3]), ' one dollar bills, ',
              str(distchange[4]), ' quarters, ',
              str(distchange[5]), ' dimes, ',
              str(distchange[6]), ' nickels, and ',
              str(distchange[7]), ' pennies.')
        need = 0
        entered.append(-1*sum(entered))

def printWelcome():
    '''
    Prints a Welcome message upon initialization
    '''
    print('Welcome! Current Balance is: ', str(sum(entered)))
    pass


keyboard.on_press (on_keypress)  ########## Set callback

## While this loop runs, keep track of the change inserted, give change for choices, and tell the customer if they have enough.
while True:
    try:
        # If a key has been pressed, check if it's a key we care about
        if pushed_key:
            if pushed_key == "0":
                entered.append(0.01)
                #penny
                print ("Current Balance: ", str(sum(entered)))

            elif pushed_key == '1':
                entered.append(0.05)
                #nickel
                print ("Current Balance: ", str(sum(entered)))

            elif pushed_key == '2':
                entered.append(0.10)
                #dime
                print ("Current Balance: ", str(sum(entered)))

            elif pushed_key == '3':
                entered.append(0.25)
                #quarter
                print ("Current Balance: ", str(sum(entered)))
                       
            elif pushed_key == '4':
                entered.append(1)
                #Dollar
                print ("Current Balance: ", str(sum(entered)))
                       
            elif pushed_key == '5':
                entered.append(5)
                #5 Dollars
                print ("Current Balance: ", str(sum(entered)))
                
            elif pushed_key == '6':
                entered.append(10)
                #10 Dollars
                print ("Current Balance: ", str(sum(entered)))
                
            elif pushed_key == '7':
                entered.append(20)
                #20 Dollars
                print ("Current Balance: ", str(sum(entered)))   
                        
            elif pushed_key == 'e':
                state = 2
                print ("Please Retrieve Your Change")
                
            elif pushed_key == 'c':
                entered.append(-1.00)
                state = 2
                print ("Cuke")
                
            elif pushed_key == 'p':
                entered.append(-1.20)
                state = 2
                print ("Popsi")
                
            elif pushed_key == 's':
                entered.append(-0.85)
                state = 2
                print ("Spryte")
                
            elif pushed_key == 'd':
                entered.append(-1.10)
                state = 2
                print ("Dr. Pupper")
            
            else:
                print('ERROR: Input Unrecognized, Please Try Again.')

            pushed_key = None
    except KeyboardInterrupt:
        break
    
    if state == 0:
        #Initialize, display message and reset balance
        printWelcome()
        state = 1
        
    elif state == 1:
        # Just allow time for any of the above keyboard options, whether they 
        # keep adding change, want it back, or choose a soda
        # Because of the keyboard nature, allow most modifications to be done 
        # through the conditional keyboard while loop above for this state
        pass
        
    elif state == 2:
        # Time to distribute change
        change = sum(entered)
        getChange(change)
        state = 0
        
    else:
        # Problem Statement
        pass
    
    
