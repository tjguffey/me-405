''' 
@file Lab9.py
This file creates a control system for our platform system.

The following file defines commands that use the TouchScreen Class and the
Encoder Class to define the current orientation of the platform system, and one
that continuously updates with new system information and applies changes to
the motors with the MotorDriver Class.


@author Tyler Guffey and Tatum X. Yee
@date March 15, 2021
''' 

from MotorDriver import MotorDriver as MD
from encoder import encoder as enc
from TouchScreen import TouchScreen as TS
import utime, pyb

class BalanceBall:
    '''
    @brief      A finite state machine to Balance a Ball on a Platform
    @details    This class implements a finite state machine to control the
                operation of a motor by use of an encoder input and gain.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Constant defining State 1
    S1_UPDATE   = 1    
    
    ## Constant defining State 2
    S2_ADJUST   = 2    
    
    ## Constant defining State 3
    S3_END      = 3
    
##############################################################################    
    def __init__(self, K1, K2, K3, K4):
        '''
        @brief            Creates a BalanceBall object.
        @details          This initializes the motors, encoders, and generalizes variables.
                          A few notes; you may want to ensure that at first the motors are set to 0 duty,
                          and that the platform is initially flat so as to correctly initialize the encoders
        @param K1         An object used to modulate motor torque based on the balls velocity
        @param K2         An object used to modulate motor torque based on the platforms angular velocity
        @param K3         An object used to modulate motor torque based on the balls position
        @param K4         An object used to modulate motor torque based on the angle of the platform 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The inital load we want to start the motors at
        self.load = 0
        
        ## The gain accounting for ball velocity
        self.K1 = self.K1 # [N/s] -0.05
        
        ## The gain acounting for the platform's angular velocity
        self.K2 = self.K2 # [Nms/deg] -0.02
        
        ## The gain accounting for the ball position
        self.K3 = self.K3 #N -0.3
        
        ## The gain accounting for the angle of the platform
        self.K4 = self.K4 #Nm/deg -0.2
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() #Useful or nah?
    
        ## The time index used for velocity calculations
        self.pos_time_index = 0
        
        ## The time index used for angular velocity calculations
        self.plat_time_index = 0
        
        ## The 'initial' theta x
        self.thx = None
        
        ## The ;initial' theta y
        self.thy = None
        
        ## The 'initial' x
        self.x = None
        
        ## The 'initial' y
        self.y = None
        
        ## The contact of the ball; changes to false if contact is lost
        self.contact = True
        
# General Motor Parameters
        
        ## Motor Resistance
        self.R = 2.21   # Ohms
        
        ## Voltage Applied to Motor
        self.V_dc = 12  # Volts
        
        ## Motor Constant
        self.k_t = 13.8*1e-3 # mNm/A -> Nm/A

            
# Touch Screen Assignment
        
        ## Define xp pin of TouchScreen 
        self.xp = pyb.Pin.cpu.A7
        
        ## Define xm pin of TouchScreen
        self.xm = pyb.Pin.cpu.A1
        
        ## Define yp pin of TouchScreen
        self.yp = pyb.Pin.cpu.A0
        
        ## Define ym pin of TouchScreen
        self.ym = pyb.Pin.cpu.A6    
        
        # Define the length of the touch screen
        self.l = 186 #mm
        
        ## Define the width of the touchscreen
        self.w = 86 #mm
        
        ## Define the center of the board
        self.c = (self.l/2, self.w/2)
    
        ## Define x-calibration gain
        self.multx = self.l/4080
        
        ## Define x-calibration gain
        self.multy = self.w/4080
        
        ## Assign the TouchScreen module for later use
        self.touch = TS(self.xp, self.xm, self.yp, self.ym, self.l, self.w, self.c, self.multx, self.multy)
          

# Motor Driver Assignment

        ## nSleep Pin of both Motors
        self.pin_nSLEEP = pyb.Pin.cpu.A15
        
        ## Pin 1 for the top right motor
        self.pin_IN1 = pyb.Pin.cpu.B0
        
        ## Pin 2 for the bottom right motor
        self.pin_IN2 = pyb.Pin.cpu.B1
        
        ## Pin 1 for the bottom right motor
        self.pin_IN12 = pyb.Pin.cpu.B4 
        
        ## Pin 2 for the bottom right motor
        self.pin_IN22 = pyb.Pin.cpu.B5
        
        ## The timer set for the top right motor
        self.tim1 = 2
        
        ## The timer set for the bottom right motor
        self.tim2 = 1

        ## Create a motor object passing in the pins and timer for the y tilting motor
        self.y_tilt_motor = MD(self.pin_nSLEEP, self.pin_IN1, self.pin_IN2, self.tim1)
        self.y_tilt_motor.set_duty(self.load)
        
        ## Create a motor object passing in the pins and timer for the x tilting motor
        self.x_tilt_motor = MD(self.pin_nSLEEP, self.pin_IN12, self.pin_IN22, self.tim2)
        self.x_tilt_motor.set_duty(self.load)

# Encoder Asignment

        ## Ticks/deg Constant
        self.ratio = 1000*4*4/360   #cpr*PPR*GR*rev/deg -> [ticks/deg]

        ## Pin 1 for top right motor (ch1)
        self.pin1 = pyb.Pin(pyb.Pin.cpu.B6)
        
        ## Pin 2 for top right motor (ch2)
        self.pin2 = pyb.Pin(pyb.Pin.cpu.B7)
        
        ## Pin for bottom right motor encoder
        self.pin3 = pyb.Pin(pyb.Pin.cpu.C6)
        
        ## Pin for bottom right motor encoder
        self.pin4 = pyb.Pin(pyb.Pin.cpu.C7)
        
        ## Timer for top right motor encoder (?)
        self.tim1 = pyb.Timer(4)
        
        ## Timer for bottom right motor encoder (?)
        self.tim2 = pyb.Timer(8)
        
        ## Encoder 1, Channel 1 setup
        self.enc1ch1 = self.tim1.channel(1, pin=self.pin1, mode=pyb.Timer.ENC_A)
        
        ## Encoder 1, Channel 2 setup
        self.enc1ch2 = self.tim1.channel(2, pin=self.pin2, mode=pyb.Timer.ENC_A)
        
        ## Encoder 2, Channel 1 setup
        self.enc2ch1 = self.tim2.channel(1, pin=self.pin3, mode=pyb.Timer.ENC_B)
        
        ## Encoder 2, Channel 2 setup
        self.enc2ch2 = self.tim2.channel(2, pin=self.pin4, mode=pyb.Timer.ENC_B)
        
        ## Timer 1 Period setup
        self.period1 = self.tim1.init(prescaler=0, period=65535)
        
        ## Timer 2 Period setup
        self.period2 = self.tim2.init(prescaler=0, period=65535)
    
        ## Create an encoder object passing in the pins and timer for the x tilting motor encoder (bottom right with ports facing you)
        self.x_tilt_enc = enc(self.pin1, self.pin2, self.tim1, self.enc1ch1, self.enc1ch2, self.period1)
        
        ## Create an encoder object passing in the pins and timer for the y tilting motor encoder (top right)
        self.y_tilt_enc = enc(self.pin3, self.pin4, self.tim2, self.enc2ch1, self.enc2ch2, self.period2)
        
##############################################################################        

    def run(self,value):
        '''
        @brief     Runs one iteration of the task
        @details   BalanceBall.run() will repeatedly update values from the
                   balancing platform system and use these values to apply 
                   changes. This will go on for however many seconds specified,
                   or until Control+C is pressed
        '''
        self.curr_time = utime.ticks_us()
        if(self.state == self.S0_INIT):
            self.transitionTo(self.S1_UPDATE)
        
        ## Adjust the duty cycle input to be usable.
        elif(self.state == self.S1_UPDATE):
            if (self.curr_time-self.start_time)/1e6<value and self.contact==True:
                
                ## Update x, y, x_d, and y_d values
                self.BallPos()
                
                ## Update thx, thy, thx_d, and thy_y values
                self.PlatPos()
                
                ## After updating the ball and platform values, move on to State 2.
                self.transitionTo(self.S2_ADJUST)
            
            ## If time is over the value specified
            else:
                self.transitionTo(self.S3_END)
                
        
        ## State 2: Send the new duty cycle request to the motors
        elif(self.state == self.S2_ADJUST):
            
            ## First adjust the x tilting motor
            new_xtorque = -self.K1*self.y_d - self.K2*self.thx_d - self.K3*self.y - self.K4*self.thx
            
            ## Change the new x torque into a PWM duty
            new_xduty = self.TorDuty(new_xtorque)
            
            ## Next adjust the y tilting motor torque
            new_ytorque = -self.K1*self.x_d - self.K2*self.thy_d - self.K3*self.x - self.K4*self.thy
            
            ## Change the new y torque into a PWM duty
            new_yduty = self.TorDuty(new_ytorque)
            
            ## Set the new x motor duty according to the new values
            self.x_tilt_motor.set_duty(new_xduty)
            
            ## Set the new y motor duty according to the new values
            self.y_tilt_motor.set_duty(new_yduty)
            
            ## After sending the new duty value, transition to state 3.
            self.transitionTO(self.S1_UPDATE)
        
        ## State 3: Turn off the motors and stop the run() function
        else:
            ## Immediately shut off both motors
            self.x_tilt_motor.esc()
            
            ## Make it so that the x_tilt motor will start at a duty cycle of 0 when the nSleep pin is next activated
            self.x_tilt_motor.disable()
            
            ## Make it so that the y_tilt motor will start at a duty cycle of 0 when the nSleep pin is next activated
            self.y_tilt_motor.disable()
            
            ## Should effectively stop the BallBalance.run() function
            return
     
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


    def BallPos(self):
        '''
        @brief     Uses the touchscreen module to obtain position and speed of the ball
        '''
        ## Save the last 'time' this data was measured
        self.old_pos_time_index = self.pos_time_index
        
        ## Track the current time these measurements are taken
        self.pos_time_index = utime.ticks_us()
        
        ## Save the x,y,and z data in a variable
        self.pos = self.touch.readscreen() # Save (x,y,z) as self.pos
        
        ## If nothing is touching the screen, the ball fell off!
        if self.pos(-1) == False:
            print('ABORT!')
            self.contact = False
            # Unsure if we can use 'break' here to exit the run task, hoping this will be fast enough for someone with their hands on the keyboard to react
            
        ## If something is touching the screen, its likely the ball; continue onwards!
        else:
            ## If there is a previously recorded x position, we would like to keep it (and the y)
            if self.x():
                ## The old x position
                self.old_x = self.x
                
                ## The old y position
                self.old_y = self.y
                
                ## The new x position
                self.x = self.pos(0)
                
                ## The new y position
                self.y = self.pos(1)
                
            ## If there is not, then we havent moved and need to compensate for velocity calcs
            else:
                ## The new x position
                self.x = self.pos(0)
                
                ## The new y position
                self.y = self.pos(1)
                
                ## Call the old x the same as x so that the velocity is counted as zero
                self.old_x = self.x
                
                ## Call the old y the same as y so that the velocity is counted as zero
                self.old_y = self.y
            
            ## The new velocity in the x direction
            self.x_d = (self.x-self.old_x)/(self.pos_time_index-self.old_pos_time_index) #mm/us
            
            # Adjust the velocity to base units
            self.x_d = self.x_d*1000 #m/s
            
            ## The new velocity in the y direction
            self.y_d = (self.y-self.old_y)/(self.pos_time_index-self.old_pos_time_index) #mm/us
            
            # Adjust the velocity to base units
            self.y_d = self.y_d*1000 #m/s


    def PlatPos(self):
        '''
        @brief     Obtains Angular Position and Velocity of the Platform
        '''
        ## Save the last 'time' this data was measured
        self.old_plat_time_index = self.plat_time_index
        
        ## Track the current time these measurements are taken
        self.plat_time_index = utime.ticks_us()
        
        ## Save the xtilting motor encoder reading
        self.xplat = self.x_tilt_enc.get_position() #enc ticks from reset returned
        
        ## Save the ytilting motor encoder reading
        self.yplat = self.y_tilt_enc.get_position() #enc ticks from reset returned
        
        ## If theres a value already for theta, save the usual way 
        if self.thx():
                ## The old thx saved for angular velocity calculation
                self.old_thx = self.thx
                
                ## The old thy saved for angular velocity calculation
                self.old_thy = self.thy
                
                ## The new xtilt after adjustment into valid x tilt angle
                self.thx = self.xplat/self.ratio
                
                ## The new ytilt after adjustment into valid y tilt angle
                self.thy = self.yplat/self.ratio
                
        ## If there is not, then we havent moved and need to compensate for angular velocity calcs
        else:
                ## The new xtilt after adjustment into valid x tilt angle
                self.thx = self.xplat/self.ratio
                
                ## The new ytilt after adjustment into valid y tilt angle
                self.thy = self.yplat/self.ratio
                
                ## The old thx saved for angular velocity calculation
                self.old_thx = self.thx
                
                ## The old thy saved for angular velocity calculation
                self.old_thy = self.thy
            
        ## The new angular velocity, thx_d
        self.thx_d = (self.thx-self.old_thx)/(self.plat_time_index-self.old_plat_time_index)*1e6 #deg/s
        
        ## The new angular velocity, thy_d
        self.thy_d = (self.thy-self.old_thy)/(self.plat_time_index-self.old_plat_time_index)*1e6 #deg/s
        
    def TorDuty(self, val):
        '''
        @brief   Changes the Torque Value into a relevant Duty Cycle % for PWM
        '''
        newval = self.R*100/self.k_t/self.V_dc*val
        return(newval)

