'''
@file mcp9808.py

This file checks if the mcp9808 is connected, and reads temperature in Celsius and Fahrenheit.

@author Tyler J. Guffey and Tatum X. Yee

@copyright TG-Coding

@date February 7, 2021
'''

from pyb import I2C

class MCP:
    '''
    @brief      This module will activate the mcp and set up some commands     
    '''
    
    def __init__(self, i2c, address):   #i2c object, its address
        '''
        @brief      Creates a MCP object.
        '''
        ## The i2c entry
        self.i2c = I2C(i2c, I2C.MASTER) 
     
        ## The address where the i2c object is located on the bus
        self.address = address
     
        ## Manufacturer ID value
        self.man_id = 84
        
        ## Memory register number
        self.memory = 5
     
        ## Manufacturer ID register
        self.manid = 6
     
    def check(self):
        '''
        @brief      Checks the i2c and returns temp in C
        '''
        id_index = bytearray(self.i2c.mem_read(2, self.address, self.manid))
        if id_index[1] == self.man_id:   #definitely wrong, but this checks the manufacturer ID
            print('MCP9808 is Connected')
        else:
            print('Bug off and find the right sensor before using this module')
    
    
    def celsius(self):
        '''
        @brief      Checks the i2c and returns temp in C
        '''
        val = self.i2c.mem_read(3, self.address, self.memory)
        value = (val[0] << 8) | val[1]
        self.temp = (value & 0xFFF) / 16.0
        if value & 0x1000:
            self.temp -=256.00
        return self.temp
        
        
    def fahrenheit(self):
        '''
        @brief      Checks the i2c and returns temp in F
        '''
        val = self.i2c.mem_read(3, self.address, 5)
        value = (val[0] << 8) | val[1]
        self.temp = (value & 0xFFF) / 16.0
        if value & 0x1000:
            self.temp -=256.00
        self.temp = self.temp*9/5+32
        return self.temp
