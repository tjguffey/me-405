'''
@file plotfile.py

This file imports a file from the serial port with specified name and plots it.

@author Tyler J. Guffey, Tatum X. Yee

@copyright TG-Coding

@date February 8, 2021
'''
from matplotlib import pyplot as plt
import serial

def plot4u(time,two,three):
    '''
    @brief     Plots a graph with the format specified
    '''
    plt.figure()
    plt.plot(time, two, 'r-', time, three, 'b-')
    plt.title('Temperarure Data')
    plt.xlabel('Time [s]')
    plt.ylabel('Temperature [F]')
    plt.legend('mcu, mcp9808')
    plt.show()


## Define the serial port we will use, empty the register
ser = serial.Serial(port='/dev/tty.usbmodem14103',baudrate=115273,timeout=1)

## Create two new lists to fill with data
time_list = list()
mcu_list = list()
mcp_list = list()

## Open the .csv file   ***Don't think this will retrieve the file from the serial port, need to look into this
file = open('TempData.csv')

## Create a readout of all the csv lines
myRows = file.readlines()

## Fill the two lists with all of our data
for myRow in myRows:
    
    ## Gain only the relevant data characters
    myList = myRow.strip('\n').split(',')
    
    ## Append the time and data to their appropriate lists, change counts to volts
    time_list.append(float(myList[0])/60)   #change time from seconds to minutes
    mcu_list.append(float(myList[1]))
    mcp_list.append(float(myList[2]))

## Close the file
file.close()

## Plot the lists we just made with formatting specified in the plot4u command
plot4u(time_list, mcu_list, time_list, mcp_list)

## Close the serial line
ser.close()
