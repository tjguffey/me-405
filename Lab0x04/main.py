## @file main_file.py
# This file brings together the file for the nucleo temp sensor and the temp sensor
# module.
#
# This program will run for a specified amount of time and write the data recorded
# to .csv file. This .csv file is accessed through the nucleo and will then be plotted.
#
# File Link: https://bitbucket.org/Lykos20/me405labs/src/master/Lab%204/main_file.py
# @author Tatum X. Yee and Tyler J. Guffey
# @date 9 February 2021



from class_file import mcu_temp
from mcp_file import mcp
import pyb
import utime
from pyb import I2C
import array

## Creates ADCAll object with a resolution of 12
temp_sensor = pyb.ADCAll(12, 0x70000)

## Passing the ADCAll object into the mcu_temp class
collect_nucleo_temp = mcu_temp(temp_sensor)

## The i2c object for the mcp9808
i2c = I2C(1, I2C.MASTER)

## The mcp9808 address on the Nucleo
address = 24

## The register of the mcp9808 that records temperature
temp_reg = 5

## Assigning the mcp as a task in our mcp module
i2c_temp_sensor = mcp(i2c, address)

## Records counter value at which data collection begins
start = utime.ticks_ms()

## Checking to identify the mcp is available
i2c_temp_sensor.check(i2c)

## The current iteration of our temperature checks
iteration = 0

## Use the .csv file as our current file
with open('TempData.csv','w') as file:
    ## Run the following code for 8 hours and then stop
    while utime.ticks_diff(utime.ticks_ms(), start) < 28800000:   # 8 hrs = 28800000 ms  
        ## Calls the mcu_temp class and runs tempMeas function to collect temperatures
        try:
            #print(nucleo_temp_array)
            #print('Start Time: ' + str(start))
            #print('Current Time: ' + str(utime.ticks_ms()))
            
            ## Print the time elapsed since starting the program
            print('Time Elapsed: ' + str((utime.ticks_diff(utime.ticks_ms(), start))/1000/60/60))
            
            ## The value to use from the mcp9808
            val = i2c.mem_read(3, address, temp_reg)
            #print(mcp_temp_array)
            #print(time_array)
            
            ## Write the time and temperature data to the .csv file
            file.write('{:},{:},{:}\r\n'.format(utime.ticks_diff(utime.ticks_ms(), start), collect_nucleo_temp.tempMeas(), i2c_temp_sensor.temp_celsius(val)))
            iteration += 1
            
            ## Wait a minute before taking another set of data
            utime.sleep(60)
            
        ## Cntrl+C consideration
        except KeyboardInterrupt:
            break
            print('Control+C has been pressed and data collection has ended')
            
    print('Temperature data collection complete!')
        