## @file Encoder.py
#
# This file serves as the encoder for our Term Project that keeps track of the
# position of the motor rotation. This class will work in conjunction with the
# motor class for our Term Project.
#
# File Link: 
# @author Tatum X. Yee and Tyler J. Guffey
# @date March 14, 2021

import pyb
import utime

## This Encoder class keeps track of the position of the motor rotation.
class Encoder:
    '''
    This class consists of several methods, the first of which initializes all
    important values. There is also an update() method that updates the current
    position along with tracking the previous position. The following get_position()
    will allow the user to view the current position of the motor. The next method,
    set_position() allows the user to input a value at which they want the tracking
    to begin (if it's not the default 0). Finally, the get_delta() method takes
    the previous position and the current position and finds the difference between
    the two positions.
    '''
    
    ## Initializes the pins, channels, timers, and period for our encoders along
    #  with various position values that will continuously be updated.
    def __init__(self, pin_in1, pin_in2, timer, channel1, channel2, period):
        '''
        @brief Creates the objects needed to regularly updates motor positions
        @param pin_in1    CPU Pin 1 of the encoder
        @param pin_in2    CPU Pin 2 of the encoder
        @param timer      Time in encoder counting mode
        @param channel1   Channel 1 of the encoder
        @param channel2   Channel 2 of the encoder
        @param period     Period of the timer
        '''
        
        ## Pin 1
        self.pin_in1 = pin_in1
        ## Pin 2
        self.pin_in2 = pin_in2
        ## Timer
        self.timer = timer
        ## Channel 1 of encoder
        self.channel1 = channel1
        ## Channel 2 of encoder
        self.channel2 = channel2
        ## Period of timer
        self.period = 65535
        
        ## Current position
        self.updated_pos = 0
        ## Previous position before update
        self.previous_pos = 0
        ## Change between current and previous positions
        self.delta_pos = 0
        ## Total position that the motor has rotated about
        self.total_position = 0
        ## Value of the reset when user has not entered one
        self.reset = 0
    
    ## Updates the position of the motor's position
    def update(self):
        ## Updates the previous position to the current position before the current
        #  position gets updated again
        self.previous_pos = self.updated_pos
        ## Updates the current position
        self.updated_pos = self.timer.counter() + self.reset
    
    ## Gets the current position of the motor
    def get_position(self):
        print('I got the position! ')
        ## Calls the update() method to update the current motor position
        self.update()
        #return self.updated_pos
        #return self.previous_pos
        print('Previous_pos: ' + str(self.previous_pos))
        print('Updated_pos: ' + str(self.updated_pos))
        print(' ')
    
    ## Sets the starting position at the user's inputted value
    def set_position(self, reset):
        ## Initializes the reset value to the value inputted by user
        self.reset = reset
        ## Makes sure that the value inputted by user is valid
        if self.reset in range(0, 65535):
            ## Sets the current position to the position inputted by user
            self.updated_pos = self.reset
            return self.updated_pos
        else:
            print('Input Invalid. Plz do not try again')
    
    ## Gets the difference between the current and previous motor positions
    def get_delta(self):
        ## Returns the absolute value of the difference between the current
        #  and previous motor position
        self.delta_pos = abs(self.updated_pos - self.previous_pos)
        ## Accounts for over and underflow
        if self.delta_pos > (self.period+1)/2:
            if self.delta_pos > 0:
                self.delta_pos -= self.period+1
            else:
                self.delta_pos += self.period+1
        ## Adds the difference in position to the total position
        self.total_position += abs(self.delta_pos)
        print('Change in dat position tho: ' + str(abs(self.delta_pos)))
        #return self.delta_pos
        
if __name__ == '__main__':
    
    pin1 = pyb.Pin(pyb.Pin.cpu.B6)  # E1 CH1
    pin2 = pyb.Pin(pyb.Pin.cpu.B7)  # E1 CH2
    pin3 = pyb.Pin(pyb.Pin.cpu.C6)  # E2 CH1
    pin4 = pyb.Pin(pyb.Pin.cpu.C7)  # E2 CH2
    
    tim1 = pyb.Timer(4)
    tim2 = pyb.Timer(8)
    
    enc1ch1 = tim1.channel(1, pin=pin1, mode=pyb.Timer.ENC_A)
    enc1ch2 = tim1.channel(2, pin=pin2, mode=pyb.Timer.ENC_A)
    enc2ch1 = tim2.channel(1, pin=pin3, mode=pyb.Timer.ENC_B)
    enc2ch2 = tim2.channel(2, pin=pin4, mode=pyb.Timer.ENC_B)
    
    period1 = tim1.init(prescaler=0, period=65535)
    period2 = tim2.init(prescaler=0, period=65535)
    
    encoder1 = Encoder(pin1, pin2, tim1, enc1ch1, enc1ch2, period1)
    encoder2 = Encoder(pin3, pin4, tim2, enc2ch1, enc2ch2, period2)
    
    encoder1.set_position(700)
    for i in range(50):
        encoder1.get_position()
        encoder1.get_delta()
        encoder2.get_position()
        encoder2.get_delta()
        utime.sleep(1)
        