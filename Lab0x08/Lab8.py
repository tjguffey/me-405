'''
@file Lab8.py
This file runs through some motor processing code, and is used to test the nFault pin.

@author Tatum X. Yee and Tyler J. Guffey
@date March 9, 2021
'''

from pyb import Pin 
from pyb import ADC
import pyb, utime
from MotorDriver import MotorDriver as MD

pin_nSLEEP = pyb.Pin.cpu.A15
pin_IN1 = pyb.Pin.cpu.B0
pin_IN2 = pyb.Pin.cpu.B1
 
pin_IN12 = pyb.Pin.cpu.B4 
pin_IN22 = pyb.Pin.cpu.B5
# Create the timer channels for the different motors
tim1 = 2
tim2 = 1

# Create a motor object passing in the pins and timer
moet = MD(pin_nSLEEP, pin_IN1, pin_IN2, tim1)
moeb = MD(pin_nSLEEP, pin_IN12, pin_IN22, tim2)

# Enable the motor driver
moet.enable()
moeb.enable()

# Set the duty cycle to ---
moet.set_duty(20)
moeb.disable()

fault = Pin.cpu.B2
fault.init(mode=pyb.Pin.ANALOG)
faultADC = pyb.ADC(fault)
nfault = faultADC.read()

while True:
    nfault = faultADC.read()
    if nfault():
        moet.esc()



## Below is example code I found from         
isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
extint = pyb.ExtInt (pyb.Pin.board.PC0,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine
# After some events have happened on the pin...
print (isr_count)