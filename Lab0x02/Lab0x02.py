'''
@file Lab0x02.py

This file turns on a light at a random time and observes reaction time.

The below code turns on an led light on the Nucleo board at a random time 
interval set between 2 and 3 seconds. It then waits for the user to click the 
button on the Nucleo in order to turn it off and saves the reaction time to 
display an average later on. The code follows the format of the picture attached 
in this section.

@image html Lab0x02.jpg

@author Tyler J. Guffey

@copyright TG-Coding

@date January 24, 2021
'''

import random
import utime, pyb, keyboard, button

class realblink:
    '''
    @brief      A finite state machine to 'blink' an actual LED.
    @details    This class implements a finite state machine to manage the
                operation of an LED. This code is directly adapted from an earlier lab. 
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT= 0
    
    ## Constant defining State 1
    S1_COUNT  = 1
    
    ## Constant defining State 2
    S2_WAIT = 2
    
    ## Constant defining State 3
    S3_RECORD = 3
    
    ## Constant defining State 4
    S4_DISPLAY = 4
    
    def __init__(self):#, interval, length):
        '''
        @brief      Creates a realblink object.
        '''
        ## The entered matrix to lee[ track of the button
        self.entered = []
        
        ## The matrix holding reaction times
        self.reaction_times = []
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The interval of time, in microseconds, between runs of the task 
        self.interval = int(0.1*1e6)
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0

        ## Empty the key roster
        self.pushed_key = None

        ## Light pin
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        
        ## Timer setup using Timer 2
        self.tim2 = pyb.Timer(2, freq = 20000)
        
        ## Setting up the timer channel
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        ## Start the system with the light off
        self.t2ch1.pulse_width_percent(0)     #Make sure light is off
        
        ## Setting up the button used for the pin(PC13) on pin A4 (PC1)
        self.pinC13 = pyb.Pin(pyb.pin.board.PA4, mode = pyb.Pin.OUT_PP)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## If a key or button has been pressed, check if it's a key we care about
        try:
            if pushed_key == "0" or self.pinC13.was_pressed():
                self.entered.append(1)

                pushed_key = None
                
        ## If cntrl-c is pressed, it will complete the calculation, print a message, and then break from the loop
        except KeyboardInterrupt:
            average = sum(self.reaction_times)/len(self.reaction_times)
            print('Your Average Reaction Time was: ', str(average), ' microseconds or ', str(average/1e6), ' seconds.')
            break
        
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            
            ## Run State 0 Code: Turn the light off and set up for flash
            if(self.state == self.S0_INIT):
                self.begin = utime.ticks_us()    #Time to being counting for light 
                self.chooseTime(2,3)             #Choose time between 2-3 seconds
                self.next_flash = self.wait_time*1e6+self.begin   #State time when to turn on light
                self.transitionTo(self.S1_COUNT) #To state 1
            
            ## Run State 1 Code: Wait until the time is right to turn on the light
            elif(self.state == self.S1_COUNT):
                ## Does nothing until the time checked exceeds the decided time for the flash
                if self.curr_time>self.next_flash:
                    self.t2ch1.pulse_width_percent(100) #Turn the light on
                    self.begin = utime.ticks_us()       #Choose starting point for reaction time
                    self.stopme = self.begin+1e6  #CHoose duration of time to leave light on if not turned off manually
                    self.transitionTo(self.S2_WAIT)
            
                ## Run State 2: Wait for the button to be pressed
            elif(self.state == self.S2_WAIT):
                
                if self.entered[-1] == 1:
                    self.entered.append(0) #Reset our checker back to 0
                    self.t2ch1.pulse_width_percent(0) #Turn the light off
                    self.reaction_times.append(utime.ticks_us-self.begin) #Append the latest reaction time in us to r.t. tuple
                    self.transitionTo(self.S3_RECORD)   #To State 3
                
                elif self.curr_time>self.stopme:
                    self.t2ch1.pulse_width_percent(0)   # Turn the light off
                    self.reaction_times.append(1e6) #Append the fact that there was no reaction
                    self.transitionTo(self.S3_RECORD)   # To State 3
                    
            ## Run State 3: Originally meant to record, now displays recent reaction time    
            elif(self.state == self.S3_RECORD):
                if self.reaction_time[-1] == 1e6:
                    print('Hello, McFly? Anybody Home?')
                else:
                    print(str(self.reaction_times[-1]))
                
                self.transitionTo(self.S0_INIT)
                
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
           
        else:
            # Undefined state for Error handling
            pass    
        
        ## Essentially State 4; print the stats
        print()
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

    def chooseTime(self,low, high):
        self.wait_time = random.randint(low*100, high*100)/100
        
    
    def on_keypress (thing):
        '''
        Callback which runs when the user presses a key.
    
        This command checks what key is pressed and saves the key as it is pushed.
        '''
        global pushed_key

        pushed_key = thing.name
        
        
#pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

#def onButtonPressFCN(IRQ_src):
    #print('Stop pushing my buttons!')

#ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       #pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)