'''
@file main2.py

This file is for the running of our light flashing code. It will take inputs
provided by the button and use these inputs to determine the reaction time of 
the user

@author Tyler J. Guffey

@copyright TG-Coding

@date January 25, 2021
'''

from Lab0x02 import realblink

task1 = realblink
print('Ready Player One')
while True:
    task1.run()